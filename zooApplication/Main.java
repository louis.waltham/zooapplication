package zooApplication;

import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
		
		// input data
		Animal giraffe = new Mammals("Mammal", "leafs,hay,carrots,biscuits", "Large", "Brown", "land,desert", "Giraffe");
		Animal monkey = new Mammals("Mammal", "nuts", "Small", "Brown", "forest", "Monkey");
		Animal parrot = new Mammals("Bird", "nuts,seeds,bread,fruits", "Small", "Mixed", "forest", "Parrot");
		Animal flamingo = new Mammals("bird", "insects,crusaceans,mollucs,fish", "Small", "Pink", "land", "Flamingo");
		Animal seal = new Mammals("Mammal", "fish", "Medium", "Grey", "water", "Seal");
		Animal penguin = new Mammals("Bird", "krill,fish", "small", "Black & White", "land,water", "Penguin");
		Animal crocodile = new Mammals("Reptile", "meat,fish,bird", "Medium", "green", "land,water", "Crocodile");
		Animal elephant = new Mammals("Mammal", "fruit,hay,pellets,vegetables", "Large", "grey", "land", "Elephant");
		Animal lion = new Mammals("Mammal", "meat", "Medium", "yellow & brown", "land", "Lion");
		Animal rhinoceros = new Mammals("Mammal", "grass,foliage,twigs,fruit", "large", "grey", "land", "Rhinoceros");
		Animal tiger = new Mammals("Bird", "meat", "Medium", "Orange & Black", "land", "Tiger");
		Animal polar_bear = new Mammals("Mammal", "fish", "large", "White", "arctic", "Polar Bear");
		Animal cheetah = new Mammals("Mammal", "meat", "medium", "Orange & Black", "land", "Cheetah");
		Animal zebra = new Mammals("Mammal", "grass,leaves,twigs", "medium", "Black & White", "land", "Zebra");
		Animal kangaroo = new Mammals("Mammal", "fruit,grass,leaves", "medium", "Brown", "land", "Kangaroo");
		Animal camel = new Mammals("Mammal", "grass,grains,oats", "medium", "Brown", "Desert", "Camel");
		Animal raccoon = new Mammals("Mammal", "fruits,nuts,fish,frogs,rabbits", "small", "Grey", "land", "Raccoon");
		Animal snake = new Mammals("Reptile", "mice,rabbits,fish", "small", "Mixed", "land,water,desert", "Snake");
		Animal goat = new Mammals("Mammal", "hay,grass", "medium", "mixed", "land", "Goat");
		Animal red_panda = new Mammals("Mammal", "fruit,bamboo,leaves", "small", "Red", "land,forest", "Red Panda");
		Animal pig = new Mammals("Mammal", "meat,insects", "medium", "mixed (mostly pink)", "land", "Pig");
		Animal ostrich = new Mammals("Bird", "meat,plants", "medium", "Male: Black & White Female: Light brown", "land,water", "Ostrich");
		Animal meerkat = new Mammals("Mammal", "insects", "small", "gray", "land", "Meerkat");
		Animal armadillo = new Mammals("Mammal", "insects,plants,fruit", "small to medium", "Gray & Brown", "land,water", "Armadillo");
		Animal owl = new Mammals("Bird", "meat", "small", "Gray & Brown", "land,forest", "Owl");
		Animal duck = new Mammals("Bird", "bread,insects", "small", "mixed", "land,water", "Duck");
		Animal centipede = new Mammals("Invertebrate", "insects", "small", "Brown", "land,forest", "Centipede");
		Animal jellyfish = new Mammals("Invertebrate", "plants,small fish,eggs", "small/medium", "Mixed", "Water", "Jellyfish");
		Animal lobster = new Mammals("Invertebrate", "meat", "small", "Brown", "land,water", "Lobster");
		Animal octupus = new Mammals("Invertebrate", "meat", "medium", "Mixed", "water", "Octupus");
		Animal starfish = new Mammals("Invertebrate", "mollusca", "small", "mixed", "water", "Starfish");
		Animal shark = new Mammals("Fish", "fish", "medium", "grey", "water", "Shark");
		Animal salamander = new Mammals("Amphibian", "insects", "small", "mixed", "water,land,forest", "Salamander");
		Animal frog = new Mammals("Amphibian", "insects", "small", "mixed", "water,land,forest", "Frog");

		
		
		
		// array of all animals
		ArrayList<Animal> animal = new ArrayList<>();
		animal.add(giraffe);
		animal.add(monkey);
		animal.add(parrot);
		animal.add(flamingo);
		animal.add(seal);
		animal.add(penguin);
		animal.add(crocodile);
		animal.add(elephant);
		animal.add(lion);
		animal.add(rhinoceros);
		animal.add(tiger);
		animal.add(polar_bear);
		animal.add(cheetah);
		animal.add(zebra);
		animal.add(kangaroo);
		animal.add(camel);
		animal.add(raccoon);
		animal.add(snake);
		animal.add(goat);
		animal.add(red_panda);
		animal.add(pig);
		animal.add(ostrich);
		animal.add(meerkat);
		animal.add(armadillo);
		animal.add(owl);
		animal.add(duck);
		animal.add(centipede);
		animal.add(jellyfish);
		animal.add(lobster);
		animal.add(octupus);
		animal.add(starfish);
		animal.add(shark);
		animal.add(salamander);
		animal.add(frog);
		
		// print title
		System.out.println("- - - - - - - - - - - - - - - - - - - -\n");
		System.out.println("- - - - Zoo application - - - - - - - -\n");
		System.out.println("- - - - Designed by - - - - - - - - - -\n");
		System.out.println("- - - - Louis - - - - - - - - - - - - -\n");
		System.out.println("- - - - - - - - - - - - - - - - - - - -\n");
		System.out.println("\n >>> All Zoo animals <<<\n");
		for(Animal an: animal) {
			an.GetDisplay();
			System.out.println();
		}
		
		ArrayList<Animal> Mammals = new ArrayList<Animal>();
		ArrayList<Animal> Birds = new ArrayList<Animal>();
		ArrayList<Animal> Fish = new ArrayList<Animal>();
		ArrayList<Animal> Reptile = new ArrayList<Animal>();
		ArrayList<Animal> Amphibians = new ArrayList<Animal>();
		ArrayList<Animal> Invertebrates = new ArrayList<Animal>();
		
		
		
		// Gathered the type of animal
		for(Animal an: animal) {
			switch(an.getTypeOfAnimal()) {
			case "Mammal": 
				Mammals.add(an);
				break;
				
			case "Bird": 
				Birds.add(an);
				break;
				
			case "Fish": 
				Fish.add(an);
				break;
				
			case "Reptile": 
				Reptile.add(an);
				break;
				
			case "Amphibian": 
				Amphibians.add(an);
				break;
				
			case "Invertebrate": 
				Invertebrates.add(an);
				break;
			}
		}
		
		System.out.println("- - - - - - - - - - - - - - - - - - - -\n");
		
	// display all animal group types
		System.out.println(">>> All Animal groups <<<\n");
		System.out.println("---All types of Mammals in the zoo---");
		displayParticularType(Mammals);
		System.out.println("---All types of Bird in the zoo---");
		displayParticularType(Birds);
		System.out.println("---All types of Fish in the zoo---");
		displayParticularType(Fish);
		System.out.println("---All types of reptile in the zoo---");
		displayParticularType(Reptile);
		System.out.println("---All types of Amphibians in the zoo---");
		displayParticularType(Amphibians);
		System.out.println("---All types of Invertebrates in the zoo---");
		displayParticularType(Invertebrates);
		
		System.out.println("\n- - - - - - - - - - - - - - - - - - - -");
		
		System.out.println("\n>>> All food for the animals <<<\n");
		ArrayList<String> foodRequired = new ArrayList<String>();
		for(Animal an: animal) {
			
			foodRequired.add(an.getName() + "- " + an.getFood());
		}
		
		for(String food: foodRequired) {
		System.out.println(food);
		}
		
		System.out.println("\n- - - - - - - - - - - - - - - - - - - -");
		
		System.out.println("\n>>> All enviroments <<<\n");
		
		ArrayList<String> forest = new ArrayList<String>();
		ArrayList<String> land = new ArrayList<String>();
		ArrayList<String> desert = new ArrayList<String>();
		ArrayList<String> water = new ArrayList<String>();
		ArrayList<String> arctic = new ArrayList<String>();
		
		// gets the type of environment
		for(Animal an: animal) {
			if(an.getEnviroment().contains("forest")) {
				forest.add(an.getName());
			}
			if(an.getEnviroment().contains("land")) {
				land.add(an.getName());
			}
			if(an.getEnviroment().contains("desert")) {
				desert.add(an.getName());
			}
			if(an.getEnviroment().contains("water")) {
				water.add(an.getName());
			}
			if(an.getEnviroment().contains("arctic")) {
				arctic.add(an.getName());
			}
		}
		
		
		System.out.println("--Land Environment--");
		specificEnvironment(land);
		System.out.println("--Forest Environment--");
		specificEnvironment(forest);
		System.out.println("--Water Environment--");
		specificEnvironment(water);
		System.out.println("--Desert Environment--");
		specificEnvironment(desert);
		System.out.println("--Arctic Environment--");
		specificEnvironment(arctic);

		}
	

	public static void displayParticularType(ArrayList<Animal> animals) {
		for(Animal an: animals) {
			
			System.out.println(an.getName()); 
		
			}	
	}
	
	public static void specificEnvironment(ArrayList<String> arrayList) {
		for(String s: arrayList) {
			System.out.println(s);
		}
	}

}
		
