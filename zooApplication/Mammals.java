package zooApplication;

import java.util.ArrayList;
import java.util.Iterator;

public class Mammals extends Animal{
		private ArrayList<String> display;
		
		public Mammals(String typeOfAnimal, String food, String size, String colour, String enviroment, String name) {
			super(typeOfAnimal, food, size, colour, enviroment, name);
			display = new ArrayList<String>();
			setDisplay();
		}
		
		// code to display the correct information
		public void setDisplay() {
			display.add("Animal: " + getName());
			display.add("Type of animal: " + getTypeOfAnimal());
			display.add("Food: " + getFood());
			display.add("Size: " + getSize());
			display.add("Colour: " + getColour());	
			display.add("Enviroment: " + getEnviroment());
		}
		
		
		public ArrayList<String> GetDisplay(){
			Iterator iterator = display.iterator();
			while(iterator.hasNext()) {
				System.out.println(iterator.next());
			}
			return display;
		}
	}
