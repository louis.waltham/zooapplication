package zooApplication;

import java.util.ArrayList;
import java.util.Iterator;

public class Birds extends Animal{
		private ArrayList<String> display;
		
		public Birds(String typeOfAnimal, String food, String size, String colour, String enviroment, String name) {
			super(typeOfAnimal, food, size, colour, enviroment, name);
			display = new ArrayList<String>();
			setDisplay();
		}
		
		// code to display the correct information
		public void setDisplay() {
			display.add("name: \n" + getName());
			display.add("Type of animal: \n" + getTypeOfAnimal());
			display.add("Food: \n" + getFood());
			display.add("Size: \n" + getSize());
			display.add("Colour: \n" + getColour());	
			display.add("Enviroment: \n" + getEnviroment());
		}
		
		
		public ArrayList<String> GetDisplay(){
			Iterator iterator = display.iterator();
			while(iterator.hasNext()) {
				System.out.println(iterator.next());
			}
			return display;
		}
	}
