package zooApplication;

import java.util.ArrayList;
import java.util.Iterator;

public class Invertebrates extends Animal{
		private ArrayList<String> display;
		
		public Invertebrates(String typeOfAnimal, String food, String size, String colour, String enviroment, String name) {
			super(typeOfAnimal, food, size, colour, enviroment, name);
			display = new ArrayList<String>();
			setDisplay();
		}
		
		// code to display the correct information
		public void setDisplay() {
			display.add(getName());
			display.add(getTypeOfAnimal());
			display.add(getFood());
			display.add(getSize());
			display.add(getColour());	
			display.add(getEnviroment());
		}
		
		
		public ArrayList<String> GetDisplay(){
			Iterator iterator = display.iterator();
			while(iterator.hasNext()) {
				System.out.println(iterator.next());
			}
			return display;
		}
	}
