package zooApplication;

import java.util.ArrayList;
import java.util.Iterator;


public class Animal {
	private String typeOfAnimal;
	private String food;
	private String size;
	private String colour;
	private String name;
	private String enviroment;
	private ArrayList<String> display;
	
	public Animal(String typeOfAnimal, String food, String size, String colour,String enviroment, String name) {
		this.typeOfAnimal = typeOfAnimal;
		this.food = food;
		this.size = size;
		this.colour = colour;
		this.name = name;
		this.enviroment = enviroment;
		display = new ArrayList<String>();
	}
	
	public String getTypeOfAnimal() {
		return typeOfAnimal;
	}

	public String getFood() {
		return food;
	}

	public String getSize() {
		return size;
	}

	public String getColour() {
		return colour;
	}
	
	public String getName() {
		return name;
	}
	
	public String getEnviroment() {
		return enviroment;
	}
	public void setDisplay() {
		display.add(getTypeOfAnimal());
		display.add(getFood());
		display.add(getSize());
		display.add(getColour());	
	}
	
	public ArrayList<String> GetDisplay(){
		Iterator iterator = display.iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		return display;
	}
}

